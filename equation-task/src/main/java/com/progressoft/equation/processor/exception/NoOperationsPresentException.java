package com.progressoft.equation.processor.exception;

public class NoOperationsPresentException extends Exception {
    public NoOperationsPresentException(String errorMessage)
    {
        super(errorMessage);
    }
}
