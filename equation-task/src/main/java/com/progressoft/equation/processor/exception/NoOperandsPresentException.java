package com.progressoft.equation.processor.exception;

public class NoOperandsPresentException extends Exception {
    public NoOperandsPresentException(String errorMessage)
    {
        super(errorMessage);
    }
}
