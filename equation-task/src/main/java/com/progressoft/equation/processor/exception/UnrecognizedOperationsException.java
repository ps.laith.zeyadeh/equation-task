package com.progressoft.equation.processor.exception;

public class UnrecognizedOperationsException extends Exception {
    public UnrecognizedOperationsException(String errorMessage)
    {
        super(errorMessage);
    }
}