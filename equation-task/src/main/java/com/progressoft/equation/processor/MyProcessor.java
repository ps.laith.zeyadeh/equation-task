package com.progressoft.equation.processor;

import com.progressoft.equation.processor.exception.EmptyEquationException;
import com.progressoft.equation.processor.exception.EquationNotValidException;

//FIXME: MyProcessor is not a proper name for this interface, you can change it to EquationProcessor and rename the sub class to DefaultEquationProcessor
public interface MyProcessor {
    // FIXME: rename it to processor
    // FIXME: Don't throw such generic exception since it will be ambiguous for the caller, I suggest o create super class EquationProcessorException and all other exceptions extends this one
    double processEquation(String equation) throws Exception;


}
