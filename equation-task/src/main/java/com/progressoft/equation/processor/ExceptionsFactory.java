package com.progressoft.equation.processor;

import com.progressoft.equation.processor.exception.*;

import java.util.HashMap;
import java.util.Map;

public class ExceptionsFactory {

    private final Validation validator;
    private HashMap<String, Exception> patternsToMatch;

    public ExceptionsFactory(Validation validator){
        this.validator = validator;
        initializeData();
    }

    private void  initializeData(){
        patternsToMatch = new HashMap<String, Exception>();
        patternsToMatch.put("[A-Za-z]{2,}", new InvalidVariableReference("Variable references can't be longer than 1"));
        patternsToMatch.put("[(]{1}[A-Za-z0-9]+[)]{1}", new UnrecognizedOperationsException("no enclosing paranethesis"));
        patternsToMatch.put("^[0-9]+$",  new NoOperationsPresentException("Equation contains operands without operations"));
        patternsToMatch.put("[_=\\\\\\[{\\]};:'\",<>~!@#$%?]{1,}", new UnrecognizedOperationsException("Equation can't contain special characters that don't reperesent operations"));
        patternsToMatch.put("/0", new DivisionByZeroException("Equation contains Division by 0"));

    }

    public void throwCorrespondingExceptionIfPatternMatch(String equation) throws Exception {
        // Iterating HashMap through for loop
        for (Map.Entry<String, Exception> set : patternsToMatch.entrySet()) {
            if(validator.findPattern(set.getKey())) throw set.getValue();
        }
    }
}
