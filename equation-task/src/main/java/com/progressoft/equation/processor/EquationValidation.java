package com.progressoft.equation.processor;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class EquationValidation implements Validation{

    private String equation;

    public void setEquation(String equation){
        this.equation = equation;
    }

    @Override
    public boolean checkIfEnoughOperands() { // don't wanna loop through this two times.
        int operandsCount = 0;
        int operationsCount = 0;
        StringBuilder sb = new StringBuilder(equation);

        for (int i = 0; i < sb.length(); i++) {
            if (Character.isDigit(sb.charAt(i))) operandsCount++;
            else {
                if ((sb.charAt(i) == '/' || sb.charAt(i) == '*') && sb.charAt(i+1) == '-') {
                    i++;
                    operationsCount++;
                }
            }
        }

        return operationsCount > operandsCount;
    }

    @Override
    public boolean handleConsecutiveOperations(String pattern) {
        Pattern p1 = Pattern.compile(pattern);
        Matcher m1 = p1.matcher(equation);
        while (m1.find()) {

            String group = m1.group();
            if (group.length() > 3) return true;
            //*- /-
            if (group.charAt(0) == '*' && group.charAt(1) == '-') return false;
            return group.charAt(0) != '/' || group.charAt(1) != '-';
        }
        return false;
    }

    @Override
    public boolean findPattern(String pattern) {
        return Pattern.compile(pattern).matcher(equation).find();
    }
}
