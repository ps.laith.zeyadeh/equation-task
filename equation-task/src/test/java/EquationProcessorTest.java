import com.progressoft.equation.processor.EquationProcessor;
import com.progressoft.equation.processor.EquationValidation;
import com.progressoft.equation.processor.MyProcessor;
import com.progressoft.equation.processor.exception.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

public class EquationProcessorTest {

    @Test
    public void givenEmptyString_whenProcessEquation_thenThrowEmptyStringException() {
        MyProcessor processor = new EquationProcessor(new EquationValidation());
        Assertions.assertThrows(EmptyEquationException.class, () -> processor.processEquation(null),
                "Should throw empty exception when equation is empty");
    }

    @Test
//    Instead of having multiple  "act" on this test, we can change it to parameterized test, please refer to @ParameterizedTest
    public void givenInvalidEquationLessThanThree_whenProcessEquation_thenThrowInvalidEquationException() {

        MyProcessor processor = new EquationProcessor(new EquationValidation());
        Assertions.assertThrows(EquationNotValidException.class, () -> processor.processEquation("2+"),
                "must throw exception when equation characters less 3");
        Assertions.assertThrows(EquationNotValidException.class, () -> processor.processEquation("*"),
                "must throw exception when having multiple Operations without operands");
    }
    @Test
    public void givenInvalidEquationWithConsectuveOperands_whenProcessEquation_thenThrowInvalidEquationException() {
        MyProcessor processor = new EquationProcessor(new EquationValidation());
        Assertions.assertThrows(NoOperationsPresentException.class, () -> processor.processEquation("33333"),
                "must throw exception when having multiple Operands in a row with no Operations");
    }
    @Test
    //    Instead of having multiple  "act" on this test, we can change it to parameterized test, please refer to @ParameterizedTest
    public void givenInvalidEquationWithConsectuveOperations_whenProcessEquation_thenThrowInvalidEquationException() {
        MyProcessor processor = new EquationProcessor(new EquationValidation());
        Assertions.assertThrows(ConsecutiveOperationsException.class, () -> processor.processEquation("3**3-+2"),
                "Number of Operands is less than operations number must throw exception");
        Assertions.assertThrows(ConsecutiveOperationsException.class, () -> processor.processEquation("3**3-/3"),
                "Number of Operands is less than operations number must throw exception");
        Assertions.assertThrows(ConsecutiveOperationsException.class, () -> processor.processEquation("3**3"),
                "must throw exception when having multiple Operations in a row");
    }
    @Test
    public void givenInvalidEquationWrongVariableReference_whenProcessEquation_thenThrowInvalidEquationException() {
        MyProcessor processor = new EquationProcessor(new EquationValidation());
        Assertions.assertThrows(InvalidVariableReference.class, () -> processor.processEquation("3+2+word"),
                "must throw exception when having variable names as a word instead of single letters");
    }

    @Test
    public void givenInvalidEquationPlusSignAtStartOfString_whenProcessEquation_thenThrowInvalidEquationException() {
        MyProcessor processor = new EquationProcessor(new EquationValidation());
        Assertions.assertThrows(EquationNotValidException.class, () -> processor.processEquation("+3-2"),
                "Only add '-' at start of equation to specifiy if it's negative, plus sing is useless here");
    }
    @Test
    public void givenInvalidEquationContainsSpecialChars_whenProcessEquation_thenThrowInvalidEquationException() {
        MyProcessor processor = new EquationProcessor(new EquationValidation());
        Assertions.assertThrows(UnrecognizedOperationsException.class, () -> processor.processEquation("3+2$1"),
                "must throw exception when equation contains special characters that don't repersent operations");
    }
    @Test
    public void givenInvalidEquationNotEnoughOperands_whenProcessEquation_thenThrowNotEnoughOperandsAvailable() {
        MyProcessor processor = new EquationProcessor(new EquationValidation());
        Assertions.assertThrows(NoOperandsPresentException.class, () -> processor.processEquation("1*-"),
                "Number of Operands is less than operations number must throw exception");
        Assertions.assertThrows(NoOperandsPresentException.class, () -> processor.processEquation("1*-2+"),
                "Number of Operands is less than operations number must throw exception");
        Assertions.assertThrows(NoOperandsPresentException.class, () -> processor.processEquation("1*-2+2-"),
                "Number of Operands is less than operations number must throw exception");
    }
    @Test
    public void givenInvalidEquationDivsionByZero_whenProcessEquation_thenThrowInvalidEquationException() {
        MyProcessor processor = new EquationProcessor(new EquationValidation());
        Assertions.assertThrows(DivisionByZeroException.class, () -> processor.processEquation("3+2*3/0"),
                "Must throw exception when contains Division by Zero expression");
    }
    @Test
    public void givenValidEquationLongOperationA_whenProcessEquation_checkResult() throws Exception {
        MyProcessor processor = new EquationProcessor(new EquationValidation());
        Assertions.assertEquals(-26, processor.processEquation("3+1-5*6"), "equation 3+1-5*6 must equal -26");
    }
    @Test
    public void givenValidEquationLongOperationB_whenProcessEquation_checkResult() throws Exception {

        MyProcessor processor = new EquationProcessor(new EquationValidation());
        Assertions.assertEquals(2, processor.processEquation("3*3/3+1-2"), "equation 3*3/3+1-2 must equal 2");
    }
    @Test
    public void givenValidEquationLongOperationC_whenProcessEquation_checkResult() throws Exception {

        MyProcessor processor = new EquationProcessor(new EquationValidation());
        Assertions.assertEquals(18, processor.processEquation("2*3^2"), "equation 2*3^2 must equal 18");
    }
    @Test
    public void givenValidEquationAddition_whenProcessEquation_checkResult() throws Exception {
        // If I do Addition then Subtraction, the value of sum would follow up in subtraction, discuss this
        // such as after addition sum is 5, then in subtraction it would be 5 + 3-2 since sum = sum + 3-2.
        MyProcessor processor = new EquationProcessor(new EquationValidation());
        Assertions.assertEquals(5, processor.processEquation("3+2"), "equation 3 +2 must equal 5");
    }
    @Test
    public void givenValidEquationSubtraction_whenProcessEquation_checkResult() throws Exception
    {
        MyProcessor processor = new EquationProcessor(new EquationValidation());
        Assertions.assertEquals(1, processor.processEquation("3-2"), "equation 3 -2 must equal 1");
    }
    @Test
    public void givenValidEquationMultipication_whenProcessEquation_checkResult() throws Exception {
        MyProcessor processor = new EquationProcessor(new EquationValidation());
        Assertions.assertEquals(6, processor.processEquation("3*2"), "equation 3 * 2 must equal 6");
    }
    @Test
    public void givenValidEquationDivision_whenProcessEquation_checkResult() throws Exception {
        MyProcessor processor = new EquationProcessor(new EquationValidation());
        Assertions.assertEquals(3, processor.processEquation("6/2"), "equation 6/2 must equal 3");
    }
    @Test
    public void givenValidEquationMultipicationWithNegative_whenProcessEquation_checkResult() throws Exception {
        MyProcessor processor = new EquationProcessor(new EquationValidation());
        Assertions.assertEquals(-5, processor.processEquation("-1*5"), "equation -1*5 must equal -5");
    }
    @Test
    public void givenValidEquationMultipicationAndDivisionWithNegativeAtSecondOperand_whenProcessEquation_checkResult() throws Exception {
        MyProcessor processor = new EquationProcessor(new EquationValidation());
        Assertions.assertEquals(-5, processor.processEquation("5*-1"), "equation 5*-1 must equal -5");
    }
    @Test
    public void givenValidEquationDivisionWithNegativeAtSecondOperandAB_whenProcessEquation_checkResult() throws Exception {
        MyProcessor processor = new EquationProcessor(new EquationValidation());
        Assertions.assertEquals(5, processor.processEquation("5/-1/-1"), "equation 5/-1/-1 must equal 5");
    }
    @Test
    public void givenValidEquationDivisionWithNegativeAtSecondOperandA_whenProcessEquation_checkResult() throws Exception {
        MyProcessor processor = new EquationProcessor(new EquationValidation());
        Assertions.assertEquals(5, processor.processEquation("5*-1*-1"), "equation 5*-1*-1 must equal 5");
    }

    //FIXME: The below test cases throws exception!!!! (Remove @Disabled to run it)
    @Test
    @Disabled
    public void testModulus() throws Exception {
        MyProcessor processor = new EquationProcessor(new EquationValidation());
        Assertions.assertEquals(3.33333333334, processor.processEquation("10/3"));
    }

    @Test
    public void givenValidEquationDivisionWithNegativeAtSecondOperand_whenProcessEquation_checkResult() throws Exception {
        MyProcessor processor = new EquationProcessor(new EquationValidation());
        Assertions.assertEquals(-5, processor.processEquation("5/-1"), "equation 5*-1 must equal -5");
    }

    @Test
    public void givenValidEquationAdditionWithNegative_whenProcessEquation_checkResult() throws Exception {
        MyProcessor processor = new EquationProcessor(new EquationValidation());
        Assertions.assertEquals(1, processor.processEquation("-1+2"), "equation -1+2 must equal 1");
    }
    @Test
    public void givenValidEquationWithOneParanethsis_whenProcessEquation_checkResult() throws Exception {

        MyProcessor processor = new EquationProcessor(new EquationValidation());
        Assertions.assertEquals(10, processor.processEquation("(3+2)*2"), "equation (3 +2) * 2 must equal 10");
    }
    @Test
    public void givenValidEquationWithOneParanethsisA_whenProcessEquation_checkResult() throws Exception {

        MyProcessor processor = new EquationProcessor(new EquationValidation());
        Assertions.assertEquals(10, processor.processEquation("2*(3+2)"), "equation 2*(3+2) must equal 10");
    }
    @Test
    public void testMultipleParenthesis() throws Exception {

        MyProcessor processor = new EquationProcessor(new EquationValidation());
        Assertions.assertEquals(10, processor.processEquation("2*((3+2)+1)"));
    }
}
