package com.progressoft.equation.processor;

import com.progressoft.equation.processor.exception.*;

import java.util.HashMap;
import java.util.Objects;
import java.util.Stack;

public class EquationProcessor implements MyProcessor {

    private final Stack<String> operandsStack;
    private final Stack<String> operationsStack;
    private final HashMap<String, Integer> operationsImportanceLevel;
    private double equationResult;
    private final Validation equationValidator;
    protected int lastOperationLevel;
    protected StringBuilder stringBuilder;

    public EquationProcessor(Validation equationValidator) {
        this.equationValidator = equationValidator;
        operationsImportanceLevel = initializeOperationsLevels();
        operandsStack = new Stack<String>(); //FIXME new Stack<>(); no need for <String> the compiler will infer the type
        operationsStack = new Stack<String>(); //FIXME new Stack<>(); no need for <String> the compiler will infer the type
    }

    private HashMap<String, Integer> initializeOperationsLevels() {
        //FIXME: no need for final keyword in this line
        final HashMap<String, Integer> priorites = new HashMap<String, Integer>();//FIXME new HashMap<>(); no need for <String> the compiler will infer the type
        priorites.put(")", 3);
        priorites.put("(", 3);
        priorites.put("+", 1);
        priorites.put("-", 1);
        priorites.put("*", 2);
        priorites.put("/", 2);
        priorites.put("^", 4);
        return priorites;
    }

    @Override
    public double processEquation(String equation) throws Exception {
        if (equation == null) throw new EmptyEquationException("Can't process Empty Equation");
        if (equation.length() < 3) throw new EquationNotValidException("Not enough Operands or Operations");
        validateEquation(equation);
        buildOperandsAndOperations(equation);
        return equationResult;
    }

    // Discuss if there is a better way to implement this.
    //FIXME: This is not the best approach to implement this method
    //Don't assign a value to the equation validator if you are passing it through the constructor (What if the same validator is being used by multiple processor, then there will be inconsistency
    //Suggested solutions: 1. Is to create the validator inside the constructor of this processor instead of passing it through constructor
    //2. Pass factory for this validator and create it once you validate the equation (this validator should be local variable instead of field)
    //Don't split validation into different classes, All validations should be located in the validator class
    private void validateEquation(String equation) throws Exception {
        equationValidator.setEquation(equation);
        ExceptionsFactory exceptionsFactory = new ExceptionsFactory(equationValidator);
        exceptionsFactory.throwCorrespondingExceptionIfPatternMatch(equation);
        exceptionalPatternsToMatch(equation);
    }

    private void exceptionalPatternsToMatch(String equation) throws NoOperandsPresentException, EquationNotValidException, ConsecutiveOperationsException {
        if (equationValidator.checkIfEnoughOperands())
            throw new NoOperandsPresentException("Not Enough Operands Presenet In Equation");

        if (equation.charAt(0) == '+')
            throw new EquationNotValidException("Write euqation without + sign at front, only use - incase its negative number");

        if (equationValidator.handleConsecutiveOperations("[/\\-+*]{2,}|(/-)|(/\\*)|(\\*-)"))
            throw new ConsecutiveOperationsException("Equation contains consecutive operations in a row such as ** or //");

        if (IfMultipicationDivisionAndNegative(!Character.isDigit(equation.charAt(equation.length() - 1)), equation.charAt(equation.length() - 1) != ')'))
            throw new NoOperandsPresentException("Number of Operands is less than operations number must throw exception");
    }

    // Purpose : determine what kind of character it is in the equation.
    private void buildOperandsAndOperations(String equation) {

        stringBuilder = new StringBuilder(equation);
        if (performIfSingleOperation(equation)) return;//FIXME: I would rather make this method return true/false and then decide if calling "performOperationWhenSingleOperation" or "handleEachOperation"
        //Don't let the method inside if statement do the job, if statement is used for branching the code
        handleEachOperation();
    }

    private void handleEachOperation() {
        for (int currentCharacterIndex = 0; currentCharacterIndex < stringBuilder.length(); currentCharacterIndex++) {
            char character = stringBuilder.charAt(currentCharacterIndex);

            if (addToDigitsStackIfTrue(character)) continue;
            if (addToDigitsStackIfFirstNumberIsNegative(character,currentCharacterIndex)){
                currentCharacterIndex++;
                continue;
            }
            if (pushFirstOperation(character)) continue;
            if (pushOperationToStackIfHigherThanPrevious(character));
            else {
                // once Execution is here, an operation will be performed and popped.
                String operation = operationsStack.pop();
                if (handleMultipicationAndDivisionByNegative(character, operation, currentCharacterIndex))
                {
                    currentCharacterIndex++;
                    continue;
                }

                if (operationsStack.size() > 0) lastOperationLevel = operationsImportanceLevel.get(operationsStack.peek());
                else {
                    if (pushNewOperationAndSetNewLevel(character, operation, currentCharacterIndex)) continue;
                }
                performOperation(operation);
            }
        }
        handleRemaingOperations(); // in some cases all operations will be handled here.
    }
    private boolean pushNewOperationAndSetNewLevel(char character, String operation, int nextCharIndex){

        lastOperationLevel = operationsImportanceLevel.get(String.valueOf(character));
        operationsStack.push(String.valueOf(character));
        if (IfMultipicationDivisionAndNegative(Objects.equals(operation, ")"), nextCharIndex + 1 != stringBuilder.length())) {
            return true;
        }
        return false;
    }

    private boolean handleMultipicationAndDivisionByNegative(char character, String operation, int indexOfDigitToHandle){

        if (IfMultipicationDivisionAndNegative(character == '-', (operation.equals("*") || operation.equals("/")))) // such as (1 +2 *-1) or 5*-1
        {
            getNegativeNumber(stringBuilder, indexOfDigitToHandle);
            operationsStack.push(operation);
            return true;
        }
        return false;
    }

    private boolean pushOperationToStackIfHigherThanPrevious(char character){
        if (ifIsOperationAndHigherThanPrevious(lastOperationLevel, character)){
            if (character == '(') {
                lastOperationLevel = 0;
                return true;
            } else lastOperationLevel = operationsImportanceLevel.get(String.valueOf(character));
            operationsStack.push(String.valueOf(character));
            return true;
        }
        return false;
    }
    private boolean addToDigitsStackIfFirstNumberIsNegative(char character, int currentCharacter){
        if (!Character.isDigit(character) && character == '-' && currentCharacter == 0) {
            getNegativeNumber(stringBuilder, currentCharacter);
            return true;
        }
        return false;
    }
    private boolean performIfSingleOperation(String equation) {
        if (equation.length() == 3) {
            performOperationWhenSingleOperation(equation);
            return true;
        }
        return false;
    }

    protected static boolean IfMultipicationDivisionAndNegative(boolean character, boolean operation) {
        return character && operation;
    }

    private boolean ifIsOperationAndHigherThanPrevious(int lastOperationLevel, char character) {
        return !Character.isDigit(character) && operationsStack.size() > 0
                && operationsImportanceLevel.get(String.valueOf(character)) > lastOperationLevel
                && character != ')';
    }

    private boolean addToDigitsStackIfTrue(char character) {
        if (Character.isDigit(character)) {
            operandsStack.push(String.valueOf(character));
            return true;
        }
        return false;
    }

    private void getNegativeNumber(StringBuilder stringBuilder, int i) {
        double operand = Double.parseDouble(String.valueOf(stringBuilder.charAt(i + 1)));
        operand = operand * -1;
        operandsStack.push(String.valueOf(operand));
    }

    private void handleRemaingOperations() {
        if (IfMultipicationDivisionAndNegative(operationsStack.size() > 0, operandsStack.size() > 0)) {
            // we only have to loop for as many operations are there
            for (int i = 0; i < operandsStack.size(); i++) performOperation(operationsStack.pop());
        }
    }

    private void performOperationWhenSingleOperation(String equation) {
        String[] params = equation.split("");
        getOperation(String.valueOf(params[1]), Double.parseDouble(params[0]), Double.parseDouble(params[2]));
    }

    private void performOperation(String operation) {
        double operandB = Double.parseDouble(operandsStack.pop());
        double operandA = Double.parseDouble(operandsStack.pop());

        getOperation(operation, operandA, operandB);
    }

    private void getOperation(String operation, double operandA, double operandB) {
        switch (operation) {
            case "+":
                equationResult = (operandA + operandB);
                operandsStack.push(String.valueOf(operandA + operandB));
                break;
            case "-":
                equationResult = (operandA - operandB);
                operandsStack.push(String.valueOf(operandA - operandB));
                break;
            case "*":
                equationResult = (operandA * operandB);
                operandsStack.push(String.valueOf(operandA * operandB));
                break;
            case "/":
                equationResult = (operandA / operandB);
                operandsStack.push(String.valueOf(operandA / operandB));
                break;
            case "^":
                equationResult = Math.pow(operandA, operandB);
                operandsStack.push(String.valueOf(Math.pow(operandA, operandB)));
                break;
            case "(":
                break;
            case ")":
                equationResult = Double.parseDouble(operandsStack.peek());
                break;
        }
    }
    public boolean pushFirstOperation(char character){
        if (IfMultipicationDivisionAndNegative(!Character.isDigit(character), operationsStack.size() == 0)) {
            if (character == '(') return true;
            lastOperationLevel = operationsImportanceLevel.get(String.valueOf(character));
            operationsStack.push(String.valueOf(character));
            return true;
        }
        return false;
    }
}

