package com.progressoft.equation.processor;

public interface Validation {
    boolean checkIfEnoughOperands();
    boolean handleConsecutiveOperations(String pattern);
    boolean findPattern(String pattern);
    void setEquation(String equation);
}
