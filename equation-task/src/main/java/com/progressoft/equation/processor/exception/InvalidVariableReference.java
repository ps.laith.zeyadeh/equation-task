package com.progressoft.equation.processor.exception;

public class InvalidVariableReference extends Exception{
    public InvalidVariableReference(String errorMessage)
    {
        super(errorMessage);
    }
}
