package com.progressoft.equation.processor.exception;

public class EmptyEquationException extends Exception{
    public EmptyEquationException(String errorMessage)
{
    super(errorMessage);
}
}
