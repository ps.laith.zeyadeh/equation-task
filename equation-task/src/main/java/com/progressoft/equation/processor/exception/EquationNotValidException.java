package com.progressoft.equation.processor.exception;

public class EquationNotValidException extends Exception{
    public  EquationNotValidException(String errorMessage)
    {
        super(errorMessage);
    }
}
