package com.progressoft.equation.processor.exception;

public class ConsecutiveOperationsException extends Exception {
    public ConsecutiveOperationsException(String errorMessage)
    {
        super(errorMessage);
    }
}
