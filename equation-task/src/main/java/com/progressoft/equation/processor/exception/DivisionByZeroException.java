package com.progressoft.equation.processor.exception;

public class DivisionByZeroException extends Exception{
    public DivisionByZeroException(String errorMessage)
    {
        super(errorMessage);
    }
}
